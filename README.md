# Advanced Data Analysis


## Temporal Data
    • Acquisition
    • Control
    • Spatial
    • Temporal
    • Spatiotemporal databases

## Neural Networks (Deep Learning)
    • Neural Network Theory
    • Perceptron
    • Feed forward
    • Radial Basis
    • Deep feed forward
    • Convolutional networks
    • Recurrent networks
    • LSTM networks
    • Sequence-to-Sequence (Auto-Encoders)

## Reinforcement Learning
    • Definition and applications
    • Passive reinforcement learning
    • Active reinforcement learning
    • GANS (general Adversarial networks)

## Natural Language Processing
    • Semantic classification 
        ◦ Topic classification
        ◦ Intent classification
        ◦ sentiment analysis

    • Semantic extraction
        ◦ Keyword extraction
        ◦ Entity extraction

    • Automatic Ticket Classification
